const { User } = require("../models");
const passport = require("../lib/passport");

const format = (user) => {
  const { id, username } = user;

  return {
    id,
    username,
    token: user.generateToken(),
  };
};

// ----------------------------------------------------------------------------------------------

exports.login = (reg, res) => {
  User.authentication(reg.body)
    .then((data) => {
      res.json({ status: "Login Success", data: format(data) });
    })
    .catch((err) => {
      res.status(500).json({ status: "Login Failed", msg: err.message });
    });
};

// -----------------------------------------------------------------------------------------------

exports.register = (reg, res) => {
  User.register(reg.body)
    .then((data) => {
      res.json({ status: "Register Success", data });
    })
    .catch((err) => {
      res.status(500).json({ status: "Register Failed", msg: err.message });
    });
};

// -----------------------------------------------------------------------------------------------

exports.getData = (reg, res) => {
  User.findAll().then((hasil) => {
    res.json({ message: "Fetch All Success", data: hasil });
  });
};

// -----------------------------------------------------------------------------------------------

exports.whoami = (reg, res) => {
  const currentUser = reg.user;

  res.json(currentUser);
};
