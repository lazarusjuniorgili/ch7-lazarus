const { Room, User } = require("../models");

exports.get = (reg, res) => {
  Room.findAll({ Include: ["user"] }).then((result) => {
    res.json({ Status: "Fetch Success", result });
  });
};

exports.create = (reg, res) => {
  Room.create(reg.body).then((result) => {
    res.json({ Status: "Create Success", result });
  });
};
