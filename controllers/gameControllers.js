const { Game, Room } = require("../models");

const findWinner = (first, second) => {
  if (first === "rock") {
    if (second === "rock") {
      return "draw";
    }
    if (second === "scissor") {
      return "win";
    }
    if (second === "paper") {
      return "lose";
    }
  }

  if (first === "scissor") {
    if (second === "rock") {
      return "lose";
    }
    if (second === "scissor") {
      return "draw";
    }
    if (second === "paper") {
      return "win";
    }
  }

  if (first === "paper") {
    if (second === "rock") {
      return "win";
    }
    if (second === "scissor") {
      return "lose";
    }
    if (second === "paper") {
      return "draw";
    }
  }
};

const random = () => {
  var result = "";
  var characters = ["rock", "papper", "scissor"];
  var characterLenght = characters.length;

  for (var i = 0; i < 1; i++) {
    result += characters[Math.floor(Math.random() * characterLenght)];
  }
  return result;
};

exports.multiplayer = async (reg, res) => {
  const { user_id, room_id, result } = reg.body;

  const isNewGame = await Game.findAll({ where: { Roomid: room_id } });
  const isUserExist = await Game.findOne({
    where: { Roomid: room_id, Userid: user_id },
  });

  if (isNewGame.length === 0) {
    Game.create({ Userid: user_id, Roomid: room_id, result })
      .then((respon) => {
        res.status(201).json({ status: "Create Game Success", respon });
      })
      .catch((err) => {
        res.status(400).json({ status: "Create Game Failed", message: err });
      });
  } else if (isNewGame.length === 1) {
    if (isNewGame.length === 1) {
      if (!isUserExist) {
        const gameResult = findWinner(isNewGame[0].result, result);

        Game.create({ Userid: user_id, Roomid: room_id, result })
          .then(() => {
            Room.update({ result: gameResult, win_player: isNewGame[0].user_id }, { where: { id: room_id } })
              .then(() => {
                res.json({ result: `Player One ${gameResult}` });
              })
              .catch((err) => {
                res.status(400).json({ status: "Create Game Failed", message: err });
              });
          })
          .catch((err) => {
            res.status(400).json({
              status: "Create Game Failed",
              message: "Create Game Failed",
            });
          });
      } else {
        res.status(400).json({
          status: "Create Game Failed",
          message: "This player is inserted",
        });
      }
    }
  } else {
    res.status(400).json({ status: "Create Game Failed", message: "This room is played" });
  }
};
