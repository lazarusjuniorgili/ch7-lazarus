exports.login = (reg, res) => {
  const title = "Login Page";

  res.render("login", { title });
};

// --------------------------------------------------------------------------------------------

exports.register = (reg, res) => {
  const title = "Register Page";

  res.render("register", { title });
};

// ---------------------------------------------------------------------------------------------

exports.home = (req, res) => {
  const title = "Hello World";
  const subTitle = "Welcome to the world!";

  res.render("index", { title, subTitle });
};
