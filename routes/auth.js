const router = require("express").Router();
const auth = require("../controllers/authControllers");
const restrict = require("../middleware/restrict");
const passport = require("passport");

router.get("/getData", auth.getData);
// router.post("/login", auth.login);
router.post("/register", auth.register);
router.get("/whoami", restrict, auth.whoami);

router.post(
  "/login",
  passport.authenticate("local", {
    successRedirect: "/page,",
    failureRedirect: "/page/login",
  })
);

module.exports = router;
