var express = require("express");
const router = express.Router();
const user = require("../controllers/userControllers");
const restrict = require("../middleware/restrict");

/* GET users listing. */
router.get("/", restrict, user.getAll);

module.exports = router;
