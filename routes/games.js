const router = require("express").Router();
const game = require("../controllers/gameControllers");

router.post("/player", game.multiplayer);

module.exports = router;
