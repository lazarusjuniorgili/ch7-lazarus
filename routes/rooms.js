const router = require("express").Router();
const room = require("../controllers/roomControllers");

router.post("/createroom", room.create);

module.exports = router;
