const router = require("express").Router();
const page = require("../controllers/pageControllers");

const restrict = (reg, res, next) => {
  if (reg.isAuthenticated()) return next();

  res.redirect("/page/login");
};

router.get("/", restrict, page.home);
router.get("/login", page.login);
router.get("/register", page.register);

module.exports = router;
